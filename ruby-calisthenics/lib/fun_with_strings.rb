module FunWithStrings
  def palindrome?
    test_str = self.gsub(/\W/, "")
    #puts test_str ; #debug
    if test_str.reverse.upcase == test_str.upcase
      true
    else
      false
    end
  end
  def count_words
    word_array = self.split(/\b/).reject {|str| " " == str}
    puts word_array ; #debug
    word_hash = Hash.new(0)
    word_array.each do |word|  
      current = word.downcase.gsub(/\W/, "") 
      word_hash[current] += 1
    end
    word_hash
  end
  def anagram_groups
    # your code here
  end
end

# make all the above functions available as instance methods on Strings:

class String
  include FunWithStrings
end
